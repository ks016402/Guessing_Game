package com.example.timth.helloandroid;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    enum BTNSTATE {GUESS, TRYAGAIN};

    //UI Elements we want to interact with
    Button enterGuessBtn;
    TextView instructionTextView;
    TextView resultTextView;
    EditText enterGuessEditText;
    TextView NoOfGuesses;
    TextView HighScore = (EditText) findViewById(R.id.HighScore);

    int Guesses = 5;
    int Score = 0;

    //Event Handlers
    View.OnClickListener guessClickListener;
    View.OnClickListener tryAgainClickListener;
    View.OnKeyListener editTextGuessListener;

    //variables we will need to work the game correctly
    Guess guessingGame;
    BTNSTATE curBtnState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //run setup
        Setup();
    }
@Override
    protected void onSaveInstanceState(Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("Guesses", Guesses);
        savedInstanceState.putInt("Score", Score);

    }
    @Override

    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        {
            Guesses = savedInstanceState.getInt("Guesses");
            Score = savedInstanceState.getInt("Score");
        }


    }

public void saveHighScores(View view)
{
    SharedPreferences sharedPref = getSharedPreferences("HighScore", Context.MODE_PRIVATE);

    SharedPreferences.Editor editor = sharedPref.edit();
    editor.putInt("Score", Score);

    editor.apply();
}

public void DisplayData(View view)
{
    SharedPreferences sharedPref = getSharedPreferences("HighScore", Context.MODE_PRIVATE);

    int hs = sharedPref.getInt("Score", 0);
HighScore.setText("Your highscore = " + hs);


}








    /**
     * Convenience function for setup (as we may need to call on it from multiple points in the app
     * lifetime
     */
    private void Setup(){

        //get our UI elements
        enterGuessBtn = (Button)findViewById(R.id.BTN_guess);
        resultTextView = (TextView)findViewById(R.id.TV_result);
        enterGuessEditText = (EditText)findViewById(R.id.ET_guess_number);
        instructionTextView = (TextView)findViewById(R.id.TV_instruction);
        NoOfGuesses = (TextView)findViewById(R.id.Guessbox);
       //NoOfGuesses.setText("The user currently has " + Guesses + " Guesses and a score of " + Score);
        // make sure our button is initialised to the GUESS state
        SetButtonState(BTNSTATE.GUESS);

        //declare our guess click listener - this will check to see if the user has guessed
        //correctly
        guessClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MakeGuess();
            }
        };

        //Try again click listener will reset the game with a new value and reset the result text to
        //the starting value
        tryAgainClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Set result to blank (i've used a dash but you could set it to whatever)
                resultTextView.setText("-");

                //Set a new value for the guessing game
                guessingGame.SetNewValue();

                //Then change the button state back to guess
                SetButtonState(BTNSTATE.GUESS);

            }
        };

        //Guess key listener is to try and get the enter button from the keyboard to
        // make the guess rather than having to input and then press the button
        editTextGuessListener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                //only attempt guess if we are in guess state
                if(curBtnState==BTNSTATE.GUESS) {
                    //only check for enter on key down
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {

                        //depending on key pressed do...
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_DPAD_CENTER:
                            case KeyEvent.KEYCODE_ENTER:
                                MakeGuess();
                                return true;
                            default:
                                break;
                        }
                    }
                }
                return false;
            }
        };

        //set key event listener for edittext
        enterGuessEditText.setOnKeyListener(editTextGuessListener);

        //initialise the Guess object, we need to get our range value from the integers.xml file
        // this is the same as with getString - we just provide the ID of our value
        int range = getResources().getInteger(R.integer.range);
        guessingGame = new Guess(range);

        //because I have a variable in my text to set I need to change the text of the instructions
        // so that it includes our range (so that %d in the string shows the actual value!
        //the getString function will automatically put the value in for us when we call it
        // like in the code below
        instructionTextView.setText(getResources().getString(R.string.Instructions,range));


        //set the result text to blank
        resultTextView.setText("-");
    }

    /**
     * Function that we can call when a guess is made by clicking a button
     */
    public void MakeGuess(){

        //Get our Guess value from the edit text by getting the input text and
        //casting the number from the string
        int guessVal = Integer.parseInt(enterGuessEditText.getText().toString());

        //get our range value again so we can use it later
        int range = getResources().getInteger(R.integer.range);

        //remove the text from the edit text so that the user knows that the guess has
        //been input
        enterGuessEditText.setText("");
if(Guesses <= 0)
{
    Guesses = 5;
   System.exit(0);


}
        //only make the guess if the value is in range
        if(guessVal>0 && guessVal<=range) {

            //pass the guess to the Guess object and change the result text
            //and button state based on the result
            if (guessingGame.MakeGuess(guessVal)) {

                //user was correct so set the result text to the win condition
                resultTextView.setText(R.string.result_win);
                //set the button state to try again so user can reset the game
                SetButtonState(BTNSTATE.TRYAGAIN);
Score++;
            } else {

                //otherwise user was incorrect so set the result text
                resultTextView.setText(R.string.result_lose);
Guesses--;
            }
            NoOfGuesses.setText("User currently has "  + Guesses + " Guesses and a score of "  + Score);
        }else{
            //if the guess is not within range we need to set the result text to let
            //the user know that the guess was invalid - again the out of range string
            //that was declared has a variable that needs to be filled so rather than just
            //passing in the ID we need to pass the value for that as well!
            resultTextView.setText(getResources().getString(R.string.result_out_of_range,range));
        }

    }

    /**
     * Function used to change the state of the button - whether it should be showing the
     * user Guess or Try Again depending on whether they have succeeded in the game or not
     */
    private void SetButtonState(BTNSTATE newState){

        //set the current button state variable
        curBtnState = newState;

        //all we are doing here is setting what the button should say and what action it
        //should perform based on which state it is being set to
        if(newState==BTNSTATE.GUESS){
            //set the button text
            enterGuessBtn.setText(R.string.button_guess);
            //set the click listener
            enterGuessBtn.setOnClickListener(guessClickListener);
        }else{
            //set the button text
            enterGuessBtn.setText(R.string.button_try_again);
            //set the try again click listener
            enterGuessBtn.setOnClickListener(tryAgainClickListener);
        }
    }



}

package com.example.timth.helloandroid;

import java.util.Random;


public class Guess {

    public int valToGuess;
    private int range;

    /**
     * defaults the range to 20 and sets a guess value
     */
    public Guess(){
        range = 20;
        SetNewValue();
    }

    /**
     * Second constructor sets the range to the input value and then sets value
     * @param rangeIn
     */
    public Guess(int rangeIn){
        this.range = rangeIn;
        SetNewValue();
    }

    /**
     * SetNewValue uses the java Random class to set the new value to guess between 1 and range
     */
    public void SetNewValue(){

        //create Random object to create our new val
        Random rng = new Random();

        //get random value, I use range -1 and then add 1 on to the generated number to ensure
        //that we get a value between 1 with no chance of getting zero
        valToGuess = 1+rng.nextInt(range-1);

    }

    /**
     * MakeGuess checks the input value against our valToGuess value
     * and returns true or false accordingly
     * @param userGuess
     * @return
     */
    public boolean MakeGuess(int userGuess){

        //assume guess is false to begin
        boolean correct = false;

        //check if guess equals our value and set boolean to true if so
        if(userGuess==valToGuess)
            correct=true;

        //return the result
        return correct;
    }


}
